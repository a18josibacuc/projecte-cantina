let baseDeDatos = [
    {
        id: 1,
        nombre: 'Pasta Mediterranea',
        precio: 2.50
    },
    {
        id: 2,
        nombre: 'Estofat de Patata',
        precio: 2.50
    },
    {
        id: 3,
        nombre: 'Amanida Verda',
        precio: 2.50
    },
    {
        id: 4,
        nombre: 'Amanida Rusa',
        precio: 2.50
    },
    {
        id: 5,
        nombre: 'Mandonguilles amb patates',
        precio: 3.50
    },
    {
        id: 6,
        nombre: 'Truita de Mongetes',
        precio: 3.50
    },
    {
        id: 7,
        nombre: 'Pit de Pollastre',
        precio: 3.50
    },
    {
        id: 8,
        nombre: 'Postre: fruita del dia',
        precio: 1.00
    }
];