let baseDeDatos = [
    {
        id: 1,
        nombre: 'Llaunes',
        precio: 1.20
    },
    {
        id: 2,
        nombre: 'Sucs',
        precio: 1.20
    },
    {
        id: 3,
        nombre: 'Vichy',
        precio: 1.40
    },
    {
        id: 4,
        nombre: 'Cacaolat',
        precio: 1.20
    },
	{
        id: 5,
        nombre: 'Got de Fruita',
        precio: 1.40
    },
	{
        id: 6,
        nombre: 'Fuet',
        precio: 1.70
    },
	{
        id: 7,
        nombre: 'Formatge sec',
        precio: 1.70
    },
	{
        id: 8,
        nombre: 'Pernil Dolç',
        precio: 1.70
    },
	{
        id: 9,
        nombre: 'Pernil Salat',
        precio: 1.90
    }
];
