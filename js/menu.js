window.onload = function() {

    // Mostra en funció de l'horari

    let avui = new Date();
    let horaActual = avui.getHours() + "" + avui.getMinutes();
    let canviMenu = "1130";
    let mati = document.getElementById("mati");
    let migdia = document.getElementById("migdia"); 
    var s = document.createElement("script");
    s.type = "text/javascript";
    let mati_block = `
    <img src="../img/mati.png" alt="mati" />

    <div class="container">
        <div class="row">
            <!-- Elementos generados a partir del JSON -->
            <main id="items" class="col-sm-8 row"></main>
            <br>
            <!-- Carrito -->
            <img src="../img/cistella.png" alt="cistella" />
            <aside class="carrito">
                <!-- Elementos del carrito -->
                <ul class="list-numbered" id="carrito" class="list-group"></ul>
                <hr>
                <!-- Precio total -->
                <p class="text-right">Total: <span id="total"></span>&euro;</p>
            </aside>
        </div>
    </div> `;
    let migdia_block = `
    <img src="../img/migdia.png" alt="migdia" />

    <div class="container">
        <div class="row">
            <!-- Elementos generados a partir del JSON -->
            <main id="items" class="col-sm-8 row"></main>
            <!-- Carrito -->
            <br>
            <img src="../img/cistella.png" alt="cistella" />
            <aside class="carrito">
                <!-- Elementos del carrito -->
                <ul class="list-numbered" id="carrito" class="list-group"></ul>
                <hr>
                <!-- Precio total -->
                <p class="text-right">Total: <span id="total"></span>&euro;</p>
            </aside>
        </div>
    </div> `;

    if(horaActual >= canviMenu){
        // Añadimos la base de datos del mediodía
        s.src = "../js/llista_menu_migdia.js";
        $("head").append(s);
        migdia.innerHTML = migdia_block;
    } else {
        // Añadimos la base de datos de la mañana
        s.src = "../js/llista_menu_mati.js";
        $("head").append(s);
        mati.innerHTML = mati_block;
    }    

    // Renderitza menú

    let $items = document.querySelector('#items');
    let carrito = [];
    let total = 0;
    let $carrito = document.querySelector('#carrito');
    let $total = document.querySelector('#total');
    // Funciones
    function renderItems () {
        for (let info of baseDeDatos) {
            // Estructura
            let miNodo = document.createElement('div');
            miNodo.classList.add('card');
            // Body
            let miNodoCardBody = document.createElement('div');
            miNodoCardBody.classList.add('card-body');
            // Titulo
            let miNodoTitle = document.createElement('h5');
            miNodoTitle.classList.add('card-title');
            miNodoTitle.textContent = info['nombre'];
            // Precio
            let miNodoPrecio = document.createElement('p');
            miNodoPrecio.classList.add('card-text');
            miNodoPrecio.textContent = info['precio'] + '€';
            // Boton 
            let miNodoBoton = document.createElement('button');
            miNodoBoton.classList.add('button_add');
            miNodoBoton.textContent = '+';
            miNodoBoton.setAttribute('marcador', info['id']);
            miNodoBoton.addEventListener('click', anyadirCarrito);
            // Insertamos
            miNodoCardBody.appendChild(miNodoTitle);
            miNodoCardBody.appendChild(miNodoPrecio);
            miNodoCardBody.appendChild(miNodoBoton);
            miNodo.appendChild(miNodoCardBody);
            $items.appendChild(miNodo);
        }
    }
    function anyadirCarrito () {
        // Anyadimos el Nodo a nuestro carrito
        carrito.push(this.getAttribute('marcador'))
        // Calculo el total
        calcularTotal();
        // Renderizamos el carrito 
        renderizarCarrito();
        // Guardamos arrays en localstorage para mostrarlos en confirmacio_comanda.php
        window.localStorage.setItem("carrito", JSON.stringify(carrito));
        window.localStorage.setItem("total", JSON.stringify(total));
    }

    function renderizarCarrito () {
        // Vaciamos todo el html
        $carrito.textContent = '';
        // Generamos los Nodos a partir de carrito
        carrito.forEach(function (item, indice) {
            // Obtenemos el item que necesitamos de la variable base de datos
            var miItem = baseDeDatos.filter(function(itemBaseDatos) {
                return itemBaseDatos['id'] == item;
            });
            // Creamos el nodo del item del carrito
            let miNodo = document.createElement('li');
            miNodo.classList.add('list-group-item', 'text-right');
            miNodo.textContent = `${miItem[0]['nombre']} - ${miItem[0]['precio']}€ `;
            // Boton de borrar
            let miBoton = document.createElement('button');
            miBoton.classList.add('button_delete');
            miBoton.textContent = 'X';
            miBoton.setAttribute('posicion', indice);
            miBoton.addEventListener('click', borrarItemCarrito);
            // Mezclamos nodos
            miNodo.appendChild(miBoton);
            $carrito.appendChild(miNodo);
        })
    }

    function borrarItemCarrito () {
        // Obtenemos la posicion que hay en el boton pulsado
        let posicion = this.getAttribute('posicion');
        // Borramos la posicion que nos interesa
        carrito.splice(posicion, 1);
        // volvemos a renderizar
        renderizarCarrito();
        // Calculamos de nuevo el precio
        calcularTotal();
    }

    function calcularTotal () {
        // Limpiamos precio anterior
        total = 0;
        // Recorremos el array del carrito
        for (let item of carrito) {
            // De cada elemento obtenemos su precio
            let miItem = baseDeDatos.filter(function(itemBaseDatos) {
                return itemBaseDatos['id'] == item;
            });
            total = total + miItem[0]['precio'];
        }
        // Renderizamos el precio en el HTML
        $total.textContent = total.toFixed(2);
    }

    // Inicio
    renderItems();

    // Inicia hora
    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('time').innerHTML =
        h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
    }
    
    function checkTime(i) {
        if (i < 10) {i = "0" + i};
        return i;
    }
    
    startTime();
};
