<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detalls de la comanda</title>
</head>
<body>
    <div class="big_container">
        <div id="header">
            <?php require "header.php" ?>
        </div>

        <div id="container_detalls">
            <div id="dades_client">
                <img src="../img/client.png" alt="client" />
                
                <form action="confirmacio_comanda.php" method="post">
                    <p>
                        <input type="text" name="nombre" placeholder="Nom i cognoms" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+" required>
                    </p>
                    <p>
                        <input type="text" name="Telefon" placeholder="Telèfon" minlength=9 maxlength=9 pattern=".{9,}" required>
                    </p>
                    <p>
                        <input type="text" name="email" placeholder="Correu electrònic" required>
                    </p>
                    <p>
                        <input id="myForm" class="buttonB" type="submit" value="ENVIAR I CONTINUAR">
                    </p>
                    <p>
                        <input id="listaItems" name="listaItems" type="hidden" value="">
                    </p>
                    <p>
                        <input id="totalItems" name="totalItems" type="hidden" value="">
                    </p>
                </form>
                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

                <script src="../js/validar_email.js"></script>
            </div>
            <div id="detalls_comanda">
                <img src="../img/comanda.png" alt="comanda" />
                <div class="lista_pedido">
                    <ol class="list-numbered" id="lista">
                    </ol>
                </div>
                <p class="total">
                <img src="../img/total.png" alt="comanda" />
                </p>
            </div>
        </div>
    
        <div class="container">
            <?php require "./footer.php" ?>
        </div>
    </div>

    <script src="../js/detalls_comanda.js"></script>
</body>
</html>
