<?php
    // Recuperamos la lista de productos de la cookie que envió Javascript
    $total_items = json_decode($_POST['totalItems']);
    $lista_items = json_decode($_POST["listaItems"], true);

    $folder = "Comanda de ".$_POST["email"]." - ".date("l jS \of F Y h:i:s A");
    mkdir ("../admin/$folder", 0755);

    $nombre_archivo = "Detalls comanda de ".$_POST["email"];
    $file = fopen("../admin/$folder/$nombre_archivo", "w");

    fwrite($file, "Client: ".$_POST["nombre"] 
    . PHP_EOL .
    "Telèfon: ". $_POST["Telefon"] 
    . PHP_EOL .
    "E-mail: ". $_POST["email"]
    . PHP_EOL .
    PHP_EOL .
    "--- DETALLS COMANDA ---");

    for ($i=0; $i < count($lista_items); $i++) { 
        fwrite($file, PHP_EOL .
        "- Producte ".($i+1).": ".$lista_items[$i]);
    }

    fwrite($file, PHP_EOL . PHP_EOL .
    "Total: ".$total_items."€");

    fclose($file); 
?>