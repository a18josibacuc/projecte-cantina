<?php
    // Comprobación cookies

    $error = "./error.php";
    if(isset($_COOKIE['compra_diaria'])) {
        header('Location: '.$error);
    }
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet' type='text/css'>
    <title>Menu</title>
    <script src="../js/menu.js"></script>
</head>
<body>
    <div class="big_container">
        <div id="header">
            <?php require "header.php" ?>
        </div>

        <div id="time"></div>

        <div class="containerItems">
            <div>
                <div id="mati"></div>

                <div id="migdia"></div>
            </div> 
            <br>
            <button class="buttonB" onclick="window.location.href='detalls_comanda.php'">CONTINUAR</button>

            <?php require "./footer.php" ?>
        <div>
    <div>
</body>
</html>