# Projecte Cantina

Repositori pel projecte transversal de 2º DAW.

# Trello

https://trello.com/b/XXgnt5mH/projecte-cantina

# Link 

http://labs.iam.cat/~a18josibacuc/projecte-cantina/

## Autors: 

* José C. Ibarra 
* Alex Estrada
* Adri Postigo
* Adam Jalich

## Objectiu:

Desenvolupar una aplicació web per la cantina de l'institut Institut Pedralbes.

## Estat:

Començant a estructurar l'aplicació. Estructura de components individuals.

23 / 10 / 2019 - Alex està acabant el menú, els estils (display: none) i pantalla finalització comanda. Adrià el formulari. José està integrant l'índex amb el menú i fent la pantalla d'error.

24 / 10 / 2019 - José C. ha integrat una cistella de la compra en el "menú.php / menú.js". Ha afegit que es pugui passar l'array de la cistella a la pantalla de confirmació. Alex i Adam estan fent el formulari.

24 / 10 / 2019 - Disseny logotip i disseny maquetació de les pantalles.

25 / 10 / 2019 - Creació del footer i implementació css/frontend.

26 / 10 / 2019 - Creació de l'arxiu que funciona com si fos una base de dades i la cookie d'avís.
