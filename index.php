<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <title>Cantina - Institut Pedralbes</title>
</head>
<body>
    <div class="big_container">
         <img id="img_header" src="img/header.png" alt="header cantina" />
        <img src="img/cafe.png" alt="cafe" id="img_index"/>
        
        <div class="container">
            <button class="myButton" onclick="window.location.href='pages/menu.php'">FER COMANDA</button>
            <button class="myButton" onclick="window.location.href='admin'">ADMINISTRADOR</button>
            <footer>
                <div class="copyright">
                    <hr width="60%"/>
                    <h3>Institut Pedralbes</h3>
                </div>
                <div>
                <p>Telèfon: 932 033 332 | Horari: 9:00 a 13:00 hores | E-mail: a8076391@xtec.cat | Copyright © 2019</p>
                </div>
            </footer>
        </div>
    <div>
    
    <!-- Aviso de cookies -->
    <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
    <script src="js/cookies.js"></script>
</body>
</html>
